function calculateScore() {
  // benmark
  var benchmark = document.getElementById("benchmark").value * 1;

  // candidate's score
  var firstScore = document.getElementById("1st-score").value * 1;
  var secondScore = document.getElementById("2nd-score").value * 1;
  var thirdScore = document.getElementById("3rd-score").value * 1;

  //score base on priority
  var regionPriorityScore =
    document.getElementById("region-priority").value * 1;
  var candidateTypePriorityScore =
    document.getElementById("candidate-type-priority").value * 1;

  var finalResult =
    firstScore +
    secondScore +
    thirdScore +
    regionPriorityScore +
    candidateTypePriorityScore;
  if (
    finalResult >= benchmark &&
    firstScore > 0 &&
    secondScore > 0 &&
    thirdScore > 0
  ) {
    document.getElementById(
      "final-result"
    ).innerText = `Bạn đã đỗ. Tổng điểm: ${finalResult}`;
  } else {
    document.getElementById(
      "final-result"
    ).innerText = `Bạn chưa đỗ. Do có điểm nhỏ hơn hoặc bằng 0, hoặc điểm của bạn thấp hơn điểm chuẩn.`;
  }
}
