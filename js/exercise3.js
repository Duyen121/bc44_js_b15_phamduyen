function getUserTaxByAnnualIncome(taxableIncome) {
  if (taxableIncome < 60e6) {
    return 0;
  } else if (taxableIncome == 60e6) {
    return 60e6 * 0.05;
  } else if (taxableIncome <= 120e6) {
    return 60e6 * 0.05 + (taxableIncome - 60e6) * 0.1;
  } else if (taxableIncome <= 210e6) {
    return 60e6 * 0.05 + 60e6 * 0.1 + (taxableIncome - 120e6) * 0.15;
  } else if (taxableIncome <= 384e6) {
    return (
      60e6 * 0.05 + 60e6 * 0.1 + 90e6 * 0.15 + (taxableIncome - 210e6) * 0.2
    );
  } else if (taxableIncome <= 624e6) {
    return (
      60e6 * 0.05 +
      60e6 * 0.1 +
      90e6 * 0.15 +
      174e6 * 0.2 +
      (taxableIncome - 384e6) * 0.25
    );
  } else if (taxableIncome <= 960e6) {
    return (
      60e6 * 0.05 +
      60e6 * 0.1 +
      90e6 * 0.15 +
      174e6 * 0.2 +
      240e6 * 0.25 +
      (taxableIncome - 624e6) * 0.3
    );
  } else {
    return (
      60e6 * 0.05 +
      60e6 * 0.1 +
      90e6 * 0.15 +
      174e6 * 0.2 +
      240e6 * 0.25 +
      336e6 * 0.3 +
      (taxableIncome - 960e6) * 0.35
    );
  }
}

function calculateTax() {
  var userName = document.getElementById("user-name-2").value;
  var annualIncome = document.getElementById("annual-income").value * 1;
  var dependentNum = document.getElementById("dependent-number").value * 1;
  var taxableIncome = annualIncome - 4e6 - dependentNum * (1e6 + 6e5);

  var personalIncomeTax = new Intl.NumberFormat("vi-VN", {
    style: "currency",
    currency: "VND",
    currencyDisplay: "code",
  }).format(getUserTaxByAnnualIncome(taxableIncome));

  if (document.querySelector('input[name="annual-income"]').value < 5e6) {
    alert("Số tổng thu nhập không hợp lệ");
    return;
  } else {
    document.getElementById(
      "user-tax"
    ).innerText = `Họ tên: ${userName}; Tiền thuế thu nhập cá nhân: ${personalIncomeTax}`;
  }
}
