function getUserPaymentFromKwNumber(kwNumber) {
  if (kwNumber <= 50) {
    return kwNumber * 500;
  } else if (kwNumber <= 100) {
    return 500 * 50 + (kwNumber - 50) * 650;
  } else if (kwNumber <= 200) {
    return 500 * 50 + 650 * 50 + (kwNumber - 100) * 850;
  } else if (kwNumber <= 350) {
    return 500 * 50 + 650 * 50 + 850 * 100 + (kwNumber - 200) * 1100;
  } else {
    return (
      500 * 50 + 650 * 50 + 850 * 100 + 1100 * 150 + (kwNumber - 350) * 1300
    );
  }
}

function calculateElectricBill() {
  var userName = document.getElementById("user-name").value;
  var kwNumber = document.getElementById("kw-number").value * 1;
  var electricPayment = new Intl.NumberFormat("vi-VN", {
    style: "currency",
    currency: "VND",
    currencyDisplay: "code",
  }).format(getUserPaymentFromKwNumber(kwNumber));

  if (document.querySelector('input[name="kw-number"]').value == "") {
    alert("Bạn hãy điền số kw hợp lệ");
    return;
  } else {
    document.getElementById(
      "electric-bill"
    ).innerText = `Họ tên: ${userName}; Tiền điện: ${electricPayment}`;
  }
}
