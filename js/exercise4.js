function getBillByCivilianType(channelNum) {
  return new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  }).format(4.5 + 20.5 + 7.5 * channelNum);
}

function getBillByEnterpriseType(channelNum, connectionNum) {
  if (connectionNum <= 10) {
    return new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    }).format(50 * channelNum + 75 + 15);
  } else {
    return new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    }).format(50 * channelNum + 75 + 5 * (connectionNum - 10) + 15);
  }
}

function calculateServiceBill() {
  var userCode = document.getElementById("user-code").value;
  var clientType = document.getElementById("client-type").value;

  var channelNum = document.getElementById("channel-num").value * 1;
  var connectionNum = document.getElementById("connection-num").value * 1;
  var serviceBill = 0;

  if (clientType == "civilian") {
    serviceBill = getBillByCivilianType(channelNum);
    return (document.getElementById(
      "service-bill"
    ).innerText = `Mã khách hàng: ${userCode}; Tiền cáp: ${serviceBill}`);
  } else if (clientType == "enterprise") {
    serviceBill = getBillByEnterpriseType(channelNum, connectionNum);
    return (document.getElementById(
      "service-bill"
    ).innerText = `Mã khách hàng: ${userCode}; Tiền cáp: ${serviceBill}`);
  } else {
    alert("Bạn hãy chọn loại khách hàng");
  }
}
